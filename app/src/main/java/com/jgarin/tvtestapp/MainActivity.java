package com.jgarin.tvtestapp;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView rv1 = (RecyclerView) findViewById(R.id.rv1);
        RecyclerView rv2 = (RecyclerView) findViewById(R.id.rv2);

        rv1.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rv1.setAdapter(new MainAdapter(10));

        rv2.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rv2.setAdapter(new MainAdapter(15));

    }

    private class MainAdapter extends RecyclerView.Adapter<MainHolder> {

        private int count;

        public MainAdapter(int count) {
            this.count = count;
        }

        @Override
        public MainHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
            return new MainHolder(view);
        }

        @Override
        public void onBindViewHolder(MainHolder holder, int position) {
            holder.onBind(position);
        }

        @Override
        public int getItemCount() {
            return count;
        }
    }

    public class MainHolder extends RecyclerView.ViewHolder {

        TextView tv;

        public MainHolder(View itemView) {
            super(itemView);
            tv = (TextView) itemView;
        }

        public void onBind(int position) {
            tv.setText("Item no " + (position + 1));
        }
    }

}
